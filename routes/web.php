<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/profile/{id}', 'ProfileController@show')->name('profile');
Route::get('/profile/{id}/edit', 'ProfileController@edit')->name('edit-profile');
Route::post('/profile/{id}/save', 'ProfileController@save')->name('save-profile');

Route::post('/profile/{id}/post', 'ProfileController@postPost')->name('post.post');
Route::post('/profile/{id}/comment', 'ProfileController@postComment')->name('comment.post');

Route::get('/all-users', 'UsersController@show')->name('all-users');

