<?php

use App\Post;
use App\User;
use App\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
        DB::table('comments')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $user = new User;
        $user->name = 'Test user';
        $user->email = 'test@test.be';
        $user->password = bcrypt('secret');
        $user->save();

        $post = new Post;
        $post->message = "This is a new message 1";
        $post->user_id = $user->id;
        $post->save();

        for ($i = 1; $i < rand(3, 8); $i++) {
            $comment = new Comment;
            $comment->name = 'Test user';
            $comment->message = "This is reply " . $i;
            $comment->post_id = $post->id;
            $comment->save();
        }

        $post = new Post;
        $post->message = "This is a new message 2";
        $post->user_id = $user->id;
        $post->save();

        for ($i = 1; $i < rand(3, 8); $i++) {
            $comment = new Comment;
            $comment->name = 'Test user';
            $comment->message = "This is reply " . $i;
            $comment->post_id = $post->id;
            $comment->save();
        }

        $post = new Post;
        $post->message = "This is a new message 3";
        $post->user_id = $user->id;
        $post->save();

        for ($i = 1; $i < rand(3, 8); $i++) {
            $comment = new Comment;
            $comment->name = 'Test user';
            $comment->message = "This is reply " . $i;
            $comment->post_id = $post->id;
            $comment->save();
        }


    }
}
