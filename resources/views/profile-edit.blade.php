@extends('layouts.app')

@section('title', 'Edit your Profile')

@section('content')

    <div class="container profilepage">
        <div class="row overview">

            <div class="col-sm-2 profile">
                @include('layouts.profile-detail')
            </div>

            <div class="col-sm-8">
                <form action="{{route('save-profile',Auth::id())}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="name">Name:</label>
                        <input class="form-control col-sm-10" type="text" name="name"
                               value="@if(isset($user->name)){{$user->name}}@endif">
                        @if ($errors->has('name'))
                            <p class="text-danger">{{$errors->first('name')}}</p>
                        @endif

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="birtday">Birthday:</label>
                        <input class="form-control col-sm-10" name="birthdate" type="text"
                               value="@if(isset($user->birthdate)) {{$user->birthdate}} @endif">
                        @if ($errors->has('birthdate'))
                            <p class="text-danger">{{$errors->first('birthdate')}} <em>(format: YYYY-MM-DD)</em></p>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="bio">Biography:</label>
                        <textarea class="form-control col-sm-10" name="bio"
                                  id="">@if(isset($user->bio)){{$user->bio}}@endif</textarea>
                        @if ($errors->has('bio'))
                            <p class="text-danger">{{$errors->first('bio')}}</p>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="country">Country:</label>
                        <input class="form-control col-sm-10" type="text" name="country"
                               value="@if(isset($user->country)){{$user->country}}@endif">
                        @if ($errors->has('country'))
                            <p class="text-danger">{{$errors->first('country')}}</p>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="image">Image:</label>
                        <input class="form-control col-sm-10" type="file" name="image" value="">
                    </div>

                    <input class="btn btn-info" type="submit" value="Save changes">

                </form>

            </div>
            <div class="col-sm-2">
            <!--   <ul class="list-group">
                    {{--HIER EEN FOREACH VOOR DE GEVOLGDE USERS (max aantal??) --}}
                    <li class="list-group-item">Who I follow</li>
                    <li class="list-group-item">Who I follow</li>
                    <li class="list-group-item">Who I follow</li>
                    <li class="list-group-item">Who I follow</li>
                    <li class="list-group-item">Who I follow</li>
                </ul>-->
            </div>
        </div>
    </div>
@endsection
