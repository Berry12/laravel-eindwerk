@extends('layouts.app')

@section('title','Profilepage')

@section('content')

    <div class="container profilepage">
        <div class="row overview">
            <div class="col-sm-2 profile">
                @include('layouts.profile-detail')
            </div>

            <div class="col-sm-8">
                <form method="post" action="{{ route('post.post', $user->id) }}" class="post-form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="postmessage">What's on your mind?</label>
                        <textarea name="message" class="form-control" id="postmessage" rows="3"
                                  placeholder='...'></textarea>
                    </div>
                    <input type="submit" class="btn btn-info mr-auto mb-2" value="Submit">
                </form>
                @php ($a = 0)
                @foreach($user->posts()->orderBy('id', 'desc')->get() as $post)
                    <div class="card text-center">
                        <div class="card-body">
                            <p>{{ $post->message }}</p>
                            <small>{{ $post->created_at }}</small>
                        </div>
                        <div class="card-footer ">
                            <p>
                                <a data-toggle="collapse" href="#collapseExample{{$a}}" aria-expanded="false"
                                   aria-controls="collapseExample{{$a}}" title="comment">
                                    <i class="fas fa-comment-alt"></i>
                                </a>
                            </p>
                        </div>
                        @foreach($post->comments as $comment)
                            <div class="card-body text-left">
                                <h5>{{ $comment->name }}</h5>
                                {{ $comment->message }}
                            </div>
                        @endforeach
                    </div>

                    <div class="collapse" id="collapseExample{{$a}}">
                        <form method="post" action="{{ route('comment.post', $post->id) }}" class="post-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="commentmessage">What's on your mind?</label>
                                <textarea name="message" class="form-control" id="commentmessage" rows="3"
                                          placeholder='...'></textarea>
                            </div>
                            <input type="submit" class="btn btn-info mr-auto mb-2" value="React">
                        </form>


                    </div>
                    @php ($a++)
                @endforeach


            </div>
            <div class="col-sm-2">
                <ul class="list-group">
                    {{--HIER EEN FOREACH VOOR DE GEVOLGDE USERS (max aantal??) --}}
                </ul>
            </div>
        </div>
    </div>
@endsection
