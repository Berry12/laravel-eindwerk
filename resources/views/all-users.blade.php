@extends('layouts.app')

@section('content')
    <div class="container all-users">
        @foreach($users as $user)
            <div class="card">
                <div class="card text-center">
                    <div class="card-body">
                        <h4><a href="#">{{ $user->name }}</a></h4>
                        <p>{{ $user->email }}</p>
                    </div>
                    <div class="card-footer ">
                        <!--  <p>
                              <i class="fas fa-user-plus" title="follow this user"></i> |
                              <i class="fas fa-user-times" title="Unfollow this user"></i>
                          </p>-->
                    </div>
                </div>
            </div>
        @endforeach


    </div>
@endsection
