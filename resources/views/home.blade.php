@extends('layouts.app')

@section('title', 'Homepage')

@section('content')
    <div class="container homepage">
        <div class="row overview">
            <div class="col-sm-2 profile">
                {{--@include('layouts.profile-detail')--}}
            </div>

            <div class="col-sm-8">
                <form class="post-form">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">What's on your mind?</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                                  placeholder='...'></textarea>
                    </div>
                    <button type="submit" class="btn btn-info mr-auto mb-2">Submit</button>
                </form>
                @foreach($posts as $post)
                    <div class="card text-center">
                        <div class="card-body">
                            {{ $post->message }}
                        </div>
                        <div class="card-footer ">
                            <!--  <p>
                                  <i class="fab fa-earlybirds"></i> 2 |
                                  <i class="fas fa-binoculars"></i> 3 |
                                  <i class="far fa-heart"></i> 4
                              </p>-->

                        </div>
                    </div>
                @endforeach

            </div>
            <div class="col-sm-2">
                <ul class="list-group">
                    {{--HIER EEN FOREACH VOOR DE GEVOLGDE USERS (max aantal??) --}}
                    @foreach($users as $user)
                        <li class="list-group-item">
                            <!-- <a href="#"><i class="far fa-heart"></i></a> -->
                            <a href="">{{ $user->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>


    </div>
@endsection
