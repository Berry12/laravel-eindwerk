<img src="{{ Storage::url($user->image) }}" alt="profilepicture">
{{--<p>{{$user->name}}</p>--}}
<p class="mx-auto">
    <!--  <a href=""><i class="fab fa-earlybirds"></i> 200</a>|
      <a href=""><i class="fas fa-binoculars"></i> 322</a> |
      <a href=""><i class="far fa-heart"></i></a> 411 -->
</p>

@if(isset($user->bio))
    <p><i class="fas fa-info"></i> {{$user->bio}}</p>
@endif

@if(isset($user->country))
    <p><i class="fas fa-globe"></i> {{$user->country}}</p>
@endif

@if(isset($user->birthdate))
    <p><i class="fas fa-birthday-cake"></i> {{$user->birthdate}}</p>
@endif


<p><em>Member since {{$user->created_at}}</em></p>


<a class="btn btn-info" href="{{route('edit-profile',Auth::id())}}">Edit profile</a>

