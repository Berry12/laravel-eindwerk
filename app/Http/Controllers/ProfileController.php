<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\ProfileformRequest;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function show($id)
    {
        // haal de posts op van de user met $username
        // haal de user op met $username

        $user = User::find($id);
        return view('profile', compact('user'));

    }

    public function edit($id)
    {
        // $user = db entry van user met $id

        $user = User::find($id);
        return view('profile-edit', compact('user'));
    }

    public function save(ProfileformRequest $request, $id)
    {
        //$user = db entry van user met $id
        // ingegeven profile info saven

        $user = User::find($id);

        $user->name = $request->input('name');
        $user->bio = $request->input('bio');
        $user->country = $request->input('country');
        $user->birthdate = $request->input('birthdate');


        if ($request->file('image') == null) {
            $user->image = $user->image;
        } else {
            $user->image = $request->file('image')->store('public/images');
        }

        $user->save();

        return redirect()->route('profile', compact('user'));
    }

    public function showProfilePage()
    {
        return view('');
    }

    public function postPost($id, Request $request)
    {
        $post = new Post;
        $post->message = $request->input('message');
        $post->user_id = $id;
        $post->save();

        return redirect()->back();
    }

    public function postComment($id, Request $request)
    {
        $comment = new Comment;
        $comment->name = Auth::user()->name;
        $comment->message = $request->input('message');
        $comment->post_id = $id;
        $comment->save();

        return redirect()->back();
    }

    public function follow($id)
    {
        $user = User::find($id);

        Auth::user()->toggleFollow($id);
    }
}
